#!/usr/bin/env bash

set -e

# Forget the postgres resources, they will be destroyed with the database server
terraform \
  -chdir=terraform \
  state rm \
  postgresql_role.version_1 \
  postgresql_role.version_2 \
  postgresql_role.version_2_admin \
  postgresql_role.version_3 \
  postgresql_role.version_3_admin \
  postgresql_role.version_4 \
  postgresql_role.version_4_admin \
  postgresql_schema.version_3 \
  postgresql_schema.version_4

# Delete the database server
terraform \
  -chdir=terraform \
  destroy \
  -auto-approve \
  -target azurerm_postgresql_flexible_server.jz_demo
  
# Recreate all the things
terraform \
  -chdir=terraform \
  apply \
  -auto-approve
