resource "azurerm_postgresql_flexible_server_database" "version_4" {
  name      = "spring_version_4"
  server_id = azurerm_postgresql_flexible_server.jz_demo.id
}

resource "random_password" "version_4" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_4" {
  name                = "version_4"
  login               = true
  password            = random_password.version_4.result
  skip_reassign_owned = true
  search_path         = [postgresql_schema.version_4.name]
}

resource "random_password" "version_4_admin" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_4_admin" {
  name                = "version_4_admin"
  login               = true
  password            = random_password.version_4_admin.result
  skip_reassign_owned = true
  search_path         = [postgresql_schema.version_4.name]
}

resource "postgresql_schema" "version_4" {
  name     = "version_4"
  database = azurerm_postgresql_flexible_server_database.version_4.name
}

resource "postgresql_grant" "version_4" {
  database    = azurerm_postgresql_flexible_server_database.version_4.name
  schema      = postgresql_schema.version_4.name
  object_type = "schema"
  privileges  = ["USAGE"]
  role        = postgresql_role.version_4.name
}

resource "postgresql_grant" "version_4_admin" {
  database    = azurerm_postgresql_flexible_server_database.version_4.name
  schema      = postgresql_schema.version_4.name
  object_type = "schema"
  privileges  = ["USAGE", "CREATE"]
  role        = postgresql_role.version_4_admin.name
}

resource "azurerm_key_vault_secret" "runtime_password" {
  key_vault_id = azurerm_key_vault.version_4.id
  name         = "spring-datasource-password"
  value        = random_password.version_4.result

  depends_on = [
    azurerm_role_assignment.me_as_officer,
  ]
}

resource "azurerm_key_vault_secret" "liquibase_password" {
  key_vault_id = azurerm_key_vault.version_4.id
  name         = "spring-liquibase-password"
  value        = random_password.version_4_admin.result

  depends_on = [
    azurerm_role_assignment.me_as_officer,
  ]
}
