resource "local_file" "tasks_json" {
  filename = "${path.module}/../.vscode/tasks.json"
  content = templatefile("${path.module}/templates/tasks.json", {
    v1_datasource_password = random_password.version_1.result
    v2_datasource_password = random_password.version_2.result
    v2_liquibase_password  = random_password.version_2_admin.result
    v3_datasource_password = random_password.version_3.result
    v3_liquibase_password  = random_password.version_3_admin.result
  })
}