resource "random_password" "administrator_password" {
  length  = 32
  special = false
}

resource "azurerm_postgresql_flexible_server" "jz_demo" {
  name                   = "spring-postgres"
  resource_group_name    = azurerm_resource_group.main.name
  location               = "norwayeast"
  administrator_login    = "jz_admin"
  administrator_password = random_password.administrator_password.result
  sku_name               = "B_Standard_B1ms"
  storage_mb             = "32768"
  version                = "14"
  zone                   = "3"
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "tf_runner" {
  name             = "terraform-runner"
  server_id        = azurerm_postgresql_flexible_server.jz_demo.id
  start_ip_address = trimspace(data.http.myip.response_body)
  end_ip_address   = trimspace(data.http.myip.response_body)
}
