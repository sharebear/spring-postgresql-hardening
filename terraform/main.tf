terraform {
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.17.1"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.19.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
    http = {
      source  = "hashicorp/http"
      version = "3.1.0"
    }
  }
}

provider "postgresql" {
  host      = azurerm_postgresql_flexible_server.jz_demo.fqdn
  port      = 5432
  username  = "jz_admin"
  password  = random_password.administrator_password.result
  superuser = false
}

provider "azurerm" {
  features {}

  subscription_id = var.azure_subscription_id
}

resource "azurerm_resource_group" "main" {
  name     = "spring-postgresql-hardening"
  location = "norwayeast"
}

data "azurerm_client_config" "current" {}
