resource "random_id" "vault_name" {
  byte_length = 3
  prefix      = "spring-postgres-"
}

resource "azurerm_key_vault" "version_4" {
  location                  = "norwayeast"
  name                      = random_id.vault_name.b64_url
  resource_group_name       = azurerm_resource_group.main.name
  sku_name                  = "standard"
  tenant_id                 = data.azurerm_client_config.current.tenant_id
  enable_rbac_authorization = true
}

resource "azurerm_role_assignment" "me_as_officer" {
  scope                = azurerm_key_vault.version_4.id
  role_definition_name = "Key Vault Secrets Officer"
  principal_id         = var.my_principal_id
}
