resource "azurerm_postgresql_flexible_server_database" "version_3" {
  name      = "spring_version_3"
  server_id = azurerm_postgresql_flexible_server.jz_demo.id
}

resource "random_password" "version_3" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_3" {
  name                = "version_3"
  login               = true
  password            = random_password.version_3.result
  skip_reassign_owned = true
  search_path         = [postgresql_schema.version_3.name]
}

resource "random_password" "version_3_admin" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_3_admin" {
  name                = "version_3_admin"
  login               = true
  password            = random_password.version_3_admin.result
  skip_reassign_owned = true
  search_path         = [postgresql_schema.version_3.name]
}

resource "postgresql_schema" "version_3" {
  name     = "version_3"
  database = azurerm_postgresql_flexible_server_database.version_3.name
}

resource "postgresql_grant" "version_3" {
  database    = azurerm_postgresql_flexible_server_database.version_3.name
  schema      = postgresql_schema.version_3.name
  object_type = "schema"
  privileges  = ["USAGE"]
  role        = postgresql_role.version_3.name
}

resource "postgresql_grant" "version_3_admin" {
  database    = azurerm_postgresql_flexible_server_database.version_3.name
  schema      = postgresql_schema.version_3.name
  object_type = "schema"
  privileges  = ["USAGE", "CREATE"]
  role        = postgresql_role.version_3_admin.name
}
