resource "azurerm_postgresql_flexible_server_database" "version_2" {
  name      = "spring_version_2"
  server_id = azurerm_postgresql_flexible_server.jz_demo.id
}

resource "random_password" "version_2" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_2" {
  name                = "version_2"
  login               = true
  password            = random_password.version_2.result
  skip_reassign_owned = true
}

resource "random_password" "version_2_admin" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_2_admin" {
  name                = "version_2_admin"
  login               = true
  password            = random_password.version_2_admin.result
  skip_reassign_owned = true
}
