resource "azurerm_postgresql_flexible_server_database" "version_1" {
  name      = "spring_version_1"
  server_id = azurerm_postgresql_flexible_server.jz_demo.id
}

resource "random_password" "version_1" {
  length  = 32
  special = false
}

resource "postgresql_role" "version_1" {
  name                = "version_1"
  login               = true
  password            = random_password.version_1.result
  skip_reassign_owned = true
}
