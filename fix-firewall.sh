#!/usr/bin/env bash

terraform \
  -chdir=terraform \
  apply \
  -auto-approve \
  -target azurerm_postgresql_flexible_server_firewall_rule.tf_runner
