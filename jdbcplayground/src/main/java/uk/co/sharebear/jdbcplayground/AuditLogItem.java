package uk.co.sharebear.jdbcplayground;

public class AuditLogItem {
    private String auditId;
    private String userId;
    private String action;

    public AuditLogItem(String auditId, String userId, String action) {
        this.auditId = auditId;
        this.userId = userId;
        this.action = action;
    }

    public String getAuditId() {
        return auditId;
    }

    public String getUserId() {
        return userId;
    }

    public String getAction() {
        return action;
    }

}
