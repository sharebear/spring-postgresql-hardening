package uk.co.sharebear.jdbcplayground;

import java.util.List;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/audit")
public class AuditController {

    private final JdbcTemplate jdbcTemplate;

    public AuditController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostMapping
    public String addLog(@RequestBody AddAuditLogRequest auditLog) {
        jdbcTemplate.update(
                "INSERT INTO audit(audit_id, user_id, action) VALUES (?, ?, ?)",
                UUID.randomUUID(), auditLog.getUserId(), auditLog.getAction());
        return "GREAT SUCCESS!";
    }

    @GetMapping
    public List<AuditLogItem> listLog() {
        return jdbcTemplate.query(
                "SELECT audit_id, user_id, action FROM audit",
                (rs, rowNum) -> new AuditLogItem(
                        rs.getString("audit_id"),
                        rs.getString("user_id"),
                        rs.getString("action")));
    }
}
