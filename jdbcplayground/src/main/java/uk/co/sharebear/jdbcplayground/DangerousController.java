package uk.co.sharebear.jdbcplayground;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DangerousController {

    private final JdbcTemplate jdbcTemplate;

    public DangerousController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostMapping("/yolo")
    public String test(@RequestBody String sqlQuery) {
        try {
            jdbcTemplate.update(sqlQuery);
            return String.format("OK [%s]", sqlQuery);
        } catch (Exception e) {
            return String.format("FAIL [%s]", e.getMessage());
        }
    }
}
