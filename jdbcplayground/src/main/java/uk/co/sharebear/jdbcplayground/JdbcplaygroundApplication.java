package uk.co.sharebear.jdbcplayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcplaygroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbcplaygroundApplication.class, args);
	}

}
